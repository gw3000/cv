# Skills Gunther Weißenbäck (2021/08)

## Programmier- und Scriptsprachen
- HTML
- CSS / SASS
- Javascript
- PHP
- Python
- Visual Basic / VBA
- Bash

## Webserver
- Apache
- NGINX
- IIS

## Frameworks
- Laravel
- Django
- FastAPI
- jQuery

## Betriebsysteme
- Linux (Arch, Debian, Ubuntu)
- Mac OS
- Microsoft Windows (Server)

## Microsoft Server
- Active Directory
- DNS
- Fileserver / Printserver
- Exchange Server

## Unified Threat Management
- Sophos UTM

## Datenbanken
- MySQL/MariaDB
- PostgreSQL
- Microsoft SQL-Server
- Oracle DBMS

## Virtualisierung / Container
- VMWare
- Proxmox
- Docker (docker-compose)

## Versionierung
- Git (github/gitlab/gitea)

## Editor
- VIM
- VSCode

# Interessen (IT-Context)
- Machine Learning
- Data Analysis
- BigData
